# README #

Are you looking to add the WOW factor at your event?

Roll Me Up create luxury ice cream rolls that are fascinating and fun to watch and deliciously creamy to eat. We can create any ice cream roll flavour you wish.

Based in the Cotswolds we are proud to offer rolled ice cream that is creamy and fresh.

Using award winning  Fresh Cotswold Ice cream with bespoke ingredients, Roll Me Up offer an ice dream experience like no other.
Deliciously creamy rolled ice cream cart hire at your wedding, certainly adds to the unique and special experience, that every wedding is. Our various wedding packages with our luxury ice cream roll cart hire will be perfect for you and we guarantee we”ll add the WOW on your special day. 

Whether you are looking to serve the guests on the entrance, whilst the photos are being taken or an afternoon or evening treat the guests will love the interaction and fun that ice cream rolls bring. We have catered at wedding venues up and down the country, take a look at some of the wedding venues here we’ve worked with.

Amazingly delicious.
Whether it’s for a wedding breakfast, a treat whilst doing the
wedding photography or an after dinner dessert whilst the
party’s still going. The magic of ice cream roll making will
certainly entertain your guests.

Hiring our ice cream roll cart for your wedding creates an immersive delicious experience

Not only do we create the most amazingly delicious rolled ice
creams, we use a very special organic Coconut cream mix to offer Vegan
Rolled ice cream cocktails too.

Looking for some Oomph? How about a Mango Mojito, or Pina Colada?
https://www.rollmeup.co.uk/